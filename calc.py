import sys
class Calculadora():
    
    def suma(num1,num2):
        resultado= int(num1+num2)
        return resultado

    def resta(num1,num2):
        resta=num1-num2
        return resta

if __name__=='__main__':

    num1=int(input('1Numero'))
    num2=int(input('2 Numero'))
    
    result1 = Calculadora.suma(num1,num2)
    result2 = Calculadora.resta(num1,num2)
    
    print(result1, " ", result2)
