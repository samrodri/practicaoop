from Persona import Persona

class Empleado(Persona):
    def __init__(self,n,d,s):
        super().__init__(n,d) 
        self.nomina = s
        
    def CalculoImpuestos(self):
        impuestos = self.nomina*0.30
        return impuestos
    
    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.CalculoImpuestos())