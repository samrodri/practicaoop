class Persona:
    def __init__(self,n,d):
        self.nombre=n
        self.DNI=d
    
class Empleado(Persona):
    def __init__(self,n,d,s):
        super().__init__(n,d) 
        self.nomina = s
        
    def CalculoImpuestos(self):
        impuestos = self.nomina*0.30
        return impuestos
    
    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.CalculoImpuestos())

class Jefe(Empleado):
    def __init__(self,n,d,s,extra=0):
        super().__init__(n,d,s)
        self.bonus=extra
    
    def CalculoImpuestos1(self):
        impuestos1= (self.nomina + self.bonus)*0.30
        return impuestos1
    
    def CalculoImpuestos(self):
        impuestos= self.CalculoImpuestos1()+ self.bonus*0.30
        return impuestos
    
    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.CalculoImpuestos())
        
EmpleadoPepe= Empleado("Pepe","1234",20000)
JefaAna= Jefe("Ana","12345",30000,2000)

print(EmpleadoPepe)
print(JefaAna)

